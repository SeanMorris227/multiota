<?php
namespace SeanMorris\Multiota;

use \SeanMorris\Ids\ChildProcess;

class Pool
{
	protected
		$dataSource
		, $mapper
		, $reducer
		, $maxMappers  = 10
		, $maxReducers = 10
		, $sourceArgs;

	public function __construct(...$args)
	{
		foreach($args as $arg)
		{
			if(is_a($arg, 'SeanMorris\Multiota\Reducer', TRUE))
			{
				$this->reducer = $arg;
				continue;
			}

			if(is_a($arg, 'SeanMorris\Multiota\Mapper', TRUE))
			{
				$this->mapper = $arg;
				continue;
			}

			if(is_a($arg, 'SeanMorris\Multiota\DataSource', TRUE))
			{
				$this->dataSource = new $arg;
				continue;
			}

			if(is_array($arg))
			{
				$this->sourceArgs = $arg;

				$arg = $arg + ['children' => 10];

				$this->maxMappers   = $arg['maxMappers']  ?? $arg['children'];
				$this->maxReducers  = $arg['maxReducers'] ?? $arg['children'];
				continue;
			}
		}
	}

	public function error($error)
	{
		fwrite(STDOUT, $error . PHP_EOL);
	}

	public function start()
	{
		$mappers   = [];
		$reducers  = [];

		$mapperId  = 0;
		$reducerId = 0;

		if($this->reducer)
		{
			$reducerClass = $this->reducer;
			$this->localReducer = new $reducerClass();
		}

		$reduceRecords = [];

		while(1)
		{
			while(!$this->dataSource->done() && count($mappers) < $this->maxMappers)
			{
				$mappers[] = new ChildProcess(
					$this->mapperCommand(++$mapperId)
					, TRUE
				);
			}

			while($mappers && count($reducers) < $this->maxReducers)
			{
				$reducers[] = new ChildProcess(
					$this->reducerCommand(++$reducerId)
					, TRUE
				);
			}

			$writableMappers = ChildProcess::writable($mappers);

			// shuffle($writableMappers);

			foreach($writableMappers as $writableMapper)
			{
				if($this->dataSource->done())
				{
					fwrite($writableMapper, "--\n");
				}
			}

			foreach($mappers as $childId => $child)
			{
				$writableReducers = ChildProcess::writable($reducers);

				if(!$writableReducers)
				{
					break;
				}

				while($record = $child->read())
				{
					shuffle($writableReducers);

					reset($writableReducers);

					while(!current($writableReducers))
					{
						next($writableReducers);
					}

					fwrite(
						current($writableReducers)
						, $record . PHP_EOL
					);
				}
			}

			$writableMappers = ChildProcess::writable($mappers);

			// shuffle($writableMappers);

			foreach($writableMappers as $writableMapper)
			{
				if($record = $this->dataSource->fetch())
				{
					$record = base64_encode(serialize($record));

					fwrite($writableMapper, $record . PHP_EOL);
				}
			}

			foreach($mappers as $childId => $child)
			{
				while($error = $child->readError())
				{
					print sprintf("m:%d:%d:%s\n", $childId, count($mappers), $error);
				}

				if($child->isDead())
				{
					$child->kill();

					unset($child, $mappers[$childId]);

					continue;
				}
			}

			foreach($reducers as $childId => $child)
			{
				while($record = $child->read())
				{
					$record = unserialize(base64_decode($record));

					$this->localReducer->process($record);

					$reducedData = $this->localReducer->get();
				}
			}

			foreach($reducers as $childId => $child)
			{
				while($error = $child->readError())
				{
					print sprintf("r:%s\n", $error);
				}

				if($child->isDead())
				{
					$child->kill();

					unset($child, $reducers[$childId]);

					continue;
				}
			}

			if(!$mappers)
			{
				$writableReducers = ChildProcess::writable($reducers);

				foreach($writableReducers as $writableReducer)
				{
					fwrite($writableReducer, "--\n");
				}

				if(!$reducers)
				{
					break;
				}
			}
		}

		$reducedData = $this->localReducer->get();

		$this->error("F:" . json_encode($reducedData));

		$done = TRUE;
	}

	protected function mapperCommand($started)
	{
		return sprintf(
			'idilic multiotaChild %s %d'
			, escapeshellarg($this->mapper)
			, $started
		);
	}

	protected function reducerCommand($started)
	{
		return sprintf(
			'idilic multiotaChild %s %d'
			, escapeshellarg($this->reducer)
			, $started
		);
	}
}
