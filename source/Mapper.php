<?php
namespace SeanMorris\Multiota;
class Mapper
{
	protected
		$child
		, $max
		, $timeout
		, $processed
		, $start
		, $done = FALSE
		, $shortClass
		, $buffer = '';

	public function __construct($child = -1)
	{
		$reflectionClass  = new \ReflectionClass(get_called_class());
		$this->shortClass = $reflectionClass->getShortName();
		$this->child      = $child;

		$log = sprintf(
			'%s process #%d starting...'
			, $this->shortClass
			, $child
		);

		// \SeanMorris\Ids\Log::debug($log . PHP_EOL);

		// stream_set_blocking(STDIN, FALSE);
		// stream_set_read_buffer(STDIN, 0);
		// stream_set_write_buffer(STDIN, 0);
		
		// stream_set_blocking(STDOUT, FALSE);
		// stream_set_read_buffer(STDOUT, 0);
		// stream_set_write_buffer(STDOUT, 0);
		
		// stream_set_blocking(STDERR, FALSE);
		// stream_set_read_buffer(STDERR, 0);
		// stream_set_write_buffer(STDERR, 0);
	}

	public function process($input)
	{
		return $input;
	}

	public function emit($data)
	{
		fwrite(STDOUT, base64_encode(serialize(
			new ReduceRecord('m_' . uniqid(), $data)
		)) . PHP_EOL);
	}

	public function reduce($record)
	{
		$this->emit($record);
	}

	public function spin()
	{
		$stdin = fopen('php://stdin', 'r');
		$child = $this->child;

		stream_set_blocking($stdin, FALSE);
		// stream_set_write_buffer($stdin, 0);

		$this->start = microtime(true);

		while(1)
		{
			$input = fgets($stdin);

			if(substr($input, -1, 1) === "\n")
			{
				$input = $this->buffer . trim($input);

				$this->buffer = '';
			}
			else
			{
				$this->buffer .= $input;

				continue;
			}			
			
			if($input == "--")
			{
				break;
			}
			else if(!$input)
			{
				continue;
			}

			$input = unserialize(base64_decode($input));
			
			if(!is_a($input, 'SeanMorris\Multiota\ReduceRecord'))
			{
				$input = new ReduceRecord('x_'.uniqid(), $input);
			}

			if(($output = $this->process($input)) !== NULL)
			{
				$this->emit($output);
			}
		}

		$this->finish();
	}

	protected function resetTimeout($timeout = NULL)
	{
		$this->start = microtime(true);

		if($timeout)
		{
			$this->timeout = $timeout;
		}
	}

	protected function timeout()
	{
		if(!$this->start)
		{
			$this->start = microtime(true);
		}

		return (microtime(true) - $this->start) > $this->timeout;
	}

	public function processed()
	{
		return $this->processed;
	}

	public function finish()
	{

	}
}
