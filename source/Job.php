<?php
namespace SeanMorris\Multiota;
class Job
{
	protected
		$mapper               = 'SeanMorris\Multiota\Mapper'
		, $unserialize        = FALSE
		, $reducer            = NULL
		, $dataSource         = 'SeanMorris\Multiota\DataSource'
		, $pool               = 'SeanMorris\Multiota\Pool'
		, $servers            = []
	;

	public function __construct(...$args)
	{
		foreach($args as $arg)
		{
			if(is_a($arg, 'SeanMorris\Multiota\Reducer', TRUE))
			{
				$this->reducer = $arg;
				continue;
			}

			if(is_a($arg, 'SeanMorris\Multiota\Mapper', TRUE))
			{
				$this->mapper = $arg;
				continue;
			}

			if(is_a($arg, 'SeanMorris\Multiota\Pool', TRUE))
			{
				$this->pool = $arg;
				continue;
			}

			if(is_a($arg, 'SeanMorris\Multiota\DataSource', TRUE))
			{
				$this->dataSource = $arg;
				continue;
			}

			if(is_array($arg))
			{
				$this->sourceArgs = $arg;

				$arg = $arg + [
					'unserialize' => $this->unserialize
					, 'servers'     => $this->servers
				];

				$this->unserialize  = $arg['unserialize'];
				$this->servers      = $arg['servers'];

				continue;
			}
		}
	}

	public function start()
	{
		$pool = new $this->pool(
			$this->dataSource
			, $this->mapper
			, $this->reducer
			, [
				'unserialize'   => $this->unserialize
				, 'servers'     => $this->servers
				, 'maxMappers'  => $this->maxMappers
				, 'maxReducers' => $this->maxReducers
			]
		);

		$pool->start();
	}
}
