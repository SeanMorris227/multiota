<?php
namespace SeanMorris\Multiota;
class DataSource
{
	protected
		$handle
		, $unserialize
		, $done
	;

	public function __construct($handle = STDIN, $unserialize = FALSE)
	{
		$this->handle      = fopen('php://STDIN', 'r');
		$this->unserialize = $unserialize;

		stream_set_blocking($this->handle, FALSE);
		stream_set_read_buffer($this->handle, 0);
	}

	public function total()
	{
		return NULL;
	}

	public function done()
	{
		return feof($this->handle);
	}

	public function fetch()
	{
		if($this->unserialize)
		{
			return unserialize(base64_decode(trim(fgets($this->handle))));
		}

		$res = fgets($this->handle);

		return trim($res);
	}
}
