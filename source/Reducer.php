<?php
namespace SeanMorris\Multiota;
class Reducer extends Mapper
{
	protected $existingData = [];

	protected function accumulate($data)
	{
		$this->existingData[$data->key()] = $data->value();
	}

	public function get()
	{
		return $this->existingData;
	}

	public function finish()
	{
		$this->emit($this->existingData);
	}

	public function process($input)
	{
		if(is_string($input))
		{
			$input = unserialize(base64_decode($input));
		}

		$this->processed++;
		
		$this->accumulate($input);
	}
}
