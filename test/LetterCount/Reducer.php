<?php
namespace SeanMorris\Multiota\Test\Lettercount;
class Reducer extends \SeanMorris\Multiota\Reducer
{
	protected function accumulate($data)
	{
		$key   = $data->key();
		$value = $data->value();

		if(!$value)
		{
			return;
		}

		foreach($value as $k => $v)
		{
			if(!isset($this->existingData[ $k ]))
			{
				$this->existingData[ $k ] = 0;
			}

			$this->existingData[ $k ] += $v;
		}

		$this->existingData['**'] = 0;

		$this->existingData['**'] = array_sum($this->existingData);

		ksort($this->existingData);

		if($this->child >= 0)
		{
			fwrite(STDERR, json_encode($this->existingData) . PHP_EOL);
		}
	}
}
