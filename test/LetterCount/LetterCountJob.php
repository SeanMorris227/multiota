<?php
namespace SeanMorris\Multiota\Test\LetterCount;
class LetterCountJob extends \SeanMorris\Multiota\Job
{
	protected
		$mapper        = 'SeanMorris\Multiota\Test\LetterCount\Mapper'
		, $reducer     = 'SeanMorris\Multiota\Test\LetterCount\Reducer'
		, $maxMappers  = 7
		, $maxReducers = 7
	;
}
