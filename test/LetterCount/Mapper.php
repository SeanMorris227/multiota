<?php
namespace SeanMorris\Multiota\Test\Lettercount;
class Mapper extends \SeanMorris\Multiota\Mapper
{
	public function process($record)
	{
		$record = str_split($record->value());
		$output = [];

		while($record)
		{
			$letter = strtoupper(array_shift($record));

			if(!preg_match('/\S/', $letter))
			{
				continue;
			}

			if(!preg_match('/[A-Z]/', $letter))
			{
				continue;
			}

			if(!isset($output[$letter]))
			{
				$output[$letter] = 0;
			}

			$output[$letter]++;
		}

		$this->processed++;

		if($output)
		{
			$this->reduce($output);
			fwrite(STDERR, json_encode($output) . PHP_EOL);
		}
	}
}
